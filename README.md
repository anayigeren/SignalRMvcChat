### ASP.Net MVC SignalR的应用

> 最近做的一个MVC项目有个模块是要使用即时通信实现弹幕效果。既要考虑通信的实时性也要考虑服务器性能和资源消耗，所幸项目对浏览器的版本没有要求。所以我最先想到用WebSocket来实现，在查询资料时， 看到 **SignalR** 这个技术，也就是本专题所讨论的对象。在拜读了 [史上最全面的SignalR系列教程-认识SignalR](https://blog.csdn.net/chinahuyong/article/details/98873807) ，算是对 SignalR有一个简单的认识，SignalR 是 .NET平台为我们提供的一种简洁高效智能的实时信息交互技术，SignalR 比 WebSocket 还要简单，这简直是我的福音。所以我直接一个Demo开始尝试。

### SignalR
关于SignalR的介绍这边就不多赘述，可以去看我上面说的这篇博文[史上最全面的SignalR系列教程-认识SignalR](https://blog.csdn.net/chinahuyong/article/details/98873807) 或者直接看Microsoft文档 [Introduction to SignalR](https://docs.microsoft.com/en-us/aspnet/signalr/overview/getting-started/introduction-to-signalr)，SignalR可用于任何即时性的功能；


### Demo ：使用SignalR和MVC 5实现实时聊天
通过第一部分的介绍，相信大家对SignalR有初步的认识，接下来直接上手一个Demo来看看(我的开发环境：VS2019 +  .net 4.6.2 + MVC 5)。步骤参考微软教程 [Tutorial: Real-time chat with SignalR 2 and MVC 5](https://docs.microsoft.com/en-us/aspnet/signalr/overview/getting-started/tutorial-getting-started-with-signalr-and-mvc)。

1. 使用Visual Studio 2019 创建一个MVC工程-**SignalRMvcChat**

2. 选择**MVC**，然后身份验证上，选择“**无身份验证**”，然后单击“**创建**”。

3. 在项目上右击-**添加新建项**，选择 **Installed** > **Visual C#** > **Web** > **SignalR and then** 选择 **SignalR Hub Class (v2)**.

4. 将类命名为ChatHub并将其添加到项目中。 此步骤将创建ChatHub.cs集线器类文件，并将一组支持SignalR的脚本文件和程序集引用添加到项目:

![](./images/1577868924803.png)

5. 用以下代码替换新建的ChatHub.cs类文件中的代码：

``` C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRMvcChat
{
    public class ChatHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addNewMessageToPage(name, message);
        }
    }
}
```
6. 在项目上，右键选择 **添加**>**类**，命名为**Startup.cs**，并用以下代码替换新建的Startup.cs类文件中的代码：

``` C#
using Owin;
using Microsoft.Owin;
[assembly: OwinStartup(typeof(SignalRMvcChat.Startup))]
namespace SignalRMvcChat
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}
```
> 注意： SignalR 框架使用Owin组件，每一个使用的OWin组件的Web框架都需要一个Startup入口类，用来声明OWin组件：
1.项目会自动扫描程序集根下的名为Startup的类，来作为入口类；
2.通过添加 [assembly: OwinStartup(typeof(SignalRChat.Startup))] 标签标记入口类；
3.如果你的启动类不在当前命名空间下，需要在Web.config中添加节点： <add key="owin:AppStartup" value="[NameSpace].Startup" />，来指定启动类；

7. 在Home控制器中添加一个方法，来返回下面聊天步骤所使用的页面：在 **HomeController.cs** 中添加以下方法：

``` C#
public ActionResult Chat()
{
	return View();
}
```

8. 在Views/Home中添加Chat视图，并用以下代码替换到新建的视图文件中：（注意：视图中所引用的signalR脚本可能跟你实际安装的版本不一样，这里需要你根据实际安装的版本进行修改）

``` javascript
@{
    ViewBag.Title = "Chat";
}
<h2>Chat</h2>
<div class="container">
    <input type="text" id="message" />
    <input type="button" id="sendmessage" value="Send" />
    <input type="hidden" id="displayname" />
    <ul id="discussion"></ul>
</div>
@section scripts {
    <!--jQuery库是必需的,默认情况下在_Layout.cshtml引用. -->
    <!--引用SignalR库. -->
    <script src="~/Scripts/jquery.signalR-2.2.2.min.js"></script>
    <!--引用自动生成的SignalR 集线器(Hub)脚本.在运行的时候在浏览器的Source下可看到. -->
    <script src="~/signalr/hubs"></script>
    <script>
        $(function () {
            // 引用自动生成的集线器代理 必须用小写字母开头.
            var chat = $.connection.chatHub;

            // 定义服务器端调用的客户端addNewMessageToPage来处理接收到的消息.
            chat.client.addNewMessageToPage = function (name, message) {
                // 将消息添加到ul上.
                $('#discussion').append('<li><strong>' + htmlEncode(name)
                    + '</strong>: ' + htmlEncode(message) + '</li>');
            };

            // 获取用户名.
            $('#displayname').val(prompt('Enter your name:', ''));

            // 设置焦点到输入框.
            $('#message').focus();

            // 开始连接服务器 done函数表明建立连接成功后为发送按钮注册了一个click事件.
            $.connection.hub.start().done(function () {
                $('#sendmessage').click(function () {
                    // 调用服务器端集线器的Send方法 也要小写开头.
                    chat.server.send($('#displayname').val(), $('#message').val());
                    // 清空输入框信息并获取焦点.
                    $('#message').val('').focus();
                });
            });
        });

        // 这个可选功能将html-encodes消息显示在页面上.
        function htmlEncode(value) {
            var encodedValue = $('<div />').text(value).html();
            return encodedValue;
        }
    </script>
}
```

#### 运行一下看看效果：

- 在工具栏中，启用“脚本调试”，然后点击“运行”按钮以在浏览器上调试（注意启动项目要设成当前的这个MVC项目）。

-进入聊天页面 ~/Home/Chat


#### 可能出现的问题：

 **The following errors occurred while attempting to load the app.**

``` 
The following errors occurred while attempting to load the app.
- No assembly found containing an OwinStartupAttribute.
- No assembly found containing a Startup or [AssemblyName].Startup class.
To disable OWIN startup discovery, add the appSetting owin:AutomaticAppStartup with a value of "false" in your web.config.
To specify the OWIN startup Assembly, Class, or Method, add the appSetting owin:AppStartup with the fully qualified startup class or configuration method name in your web.config.
```
以上的问题出现在我第一次运行的时候，看着错误提示是没有找到 OwinStartupAttribute ，然后跟着提示在web.config的appSetting中添加如下的节点：

```xml
<appSettings>
    <add key="owin:AutomaticAppStartup" value="false" />
	...
</appSettings>
```
运行居然真的就不报错，但是问题更大条了。在聊天页面并没有生成SignalR代理的hubs的脚本文件，出现了`Uncaught TypeError: Cannot read property 'client' of undefined错误`。

对比了微软官方使用 SignalR 步骤，才认识到 SignalR 框架使用Owin组件，一定要指定一个Startup入口类，我是少了以上步骤6，在添加了Startup.cs类之后，问题解决了（添加的Startup.cs类的命名空间为应用程序根目录的名称，去掉之前web.config中加入的错误代码）。对于SignalR代理对象异常，大家还可以看下这篇文章， [SignalR代理对象异常：Uncaught TypeError: Cannot read property 'client' of undefined 推出的结论](https://www.cnblogs.com/dunitian/p/5230260.html)，只要注意自动生成的集线器脚本中的代理名称和服务端方法都是小写字母开头，调用的时候使用小写就可以避免这个问题。

以上就是今天给大家的分享全部内容了，祝大家元旦快乐！

### 参考文章
- [史上最全面的SignalR系列教程-认识SignalR](https://blog.csdn.net/chinahuyong/article/details/98873807) 
- [Tutorial: Real-time chat with SignalR 2 and MVC 5](https://docs.microsoft.com/en-us/aspnet/signalr/overview/getting-started/tutorial-getting-started-with-signalr-and-mvc)
- [SignalR代理对象异常：Uncaught TypeError: Cannot read property 'client' of undefined 推出的结论](https://www.cnblogs.com/dunitian/p/5230260.html)